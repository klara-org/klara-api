#!/bin/bash

echo "[LOG] Removing a node..."
sudo -i -u postgres psql klara-api-dev -c  "SELECT * from master_remove_node('$1', $2);"

sudo ./list_workers.sh