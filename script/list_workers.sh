#!/bin/bash

echo "[LOG] Listing active nodes "
sudo -i -u postgres psql klara-api-dev -c "SELECT * FROM master_get_active_worker_nodes();"