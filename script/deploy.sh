#!/bin/bash

# Download and install Heroku CLI
# curl https://cli-assets.heroku.com/install-ubuntu.sh | sh


install_heroku() {
{
    set -e
    SUDO=''
    if [ "$(id -u)" != "0" ]; then
      SUDO='sudo'
      echo "This script requires superuser access to install apt packages."
      echo "You will be prompted for your password by sudo."
      # clear any previous sudo permission
      sudo -k
    fi

    # run inside sudo
    $SUDO sh <<SCRIPT
  set -ex

  # if apt-transport-https is not installed, clear out old sources, update, then install apt-transport-https
  dpkg -s apt-transport-https 1>/dev/null 2>/dev/null || \
    (echo "" > /etc/apt/sources.list.d/heroku.list \
      && apt-get update \
      && apt-get install -y apt-transport-https)

  # add heroku repository to apt
  echo "deb https://cli-assets.heroku.com/apt ./" > /etc/apt/sources.list.d/heroku.list

  # remove toolbelt
  (dpkg -s heroku-toolbelt 1>/dev/null 2>/dev/null && (apt-get remove -y heroku-toolbelt heroku || true)) || true

  # install heroku's release key for package verification
  curl https://cli-assets.heroku.com/apt/release.key | apt-key add -

  # update your sources
  apt-get update

  # install the toolbelt
  apt-get install -y heroku

SCRIPT
  # test the CLI
  LOCATION=$(which heroku)
  echo "heroku installed to $LOCATION"
  heroku version
}
}

verify_valid_cmd() {
    if [[ $? -eq 0 ]]; then
        echo -e $1;
    else
        echo -e $2;
        if [[ "$2" == "Heroku installation needed!" ]]; then
            install_heroku
        fi;
    fi;
}


dpkg -s heroku > /dev/null
verify_valid_cmd "You already have Heroku installed!" "Heroku installation needed!"

git remote add staging https://git.heroku.com/klara-api-staging.git &> /dev/null
verify_valid_cmd "Stage remote successfully added!" "Stage remote already exists!"

git remote add prod https://git.heroku.com/klara-api-prod.git &> /dev/null
verify_valid_cmd "Production remote successfully added!" "Production remote already exists!\nContinuing..."

git remote update
heroku login

git push -f staging master
git push -f prod master
