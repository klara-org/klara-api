#!/bin/bash
sudo service postgresql restart

if [ $(stat -c %U /var/run/postgresql) != "postgres" ] ; then
    sudo chown -R postgres /var/run/postgresql;
fi;

while ! tail /var/log/postgresql/postgresql-10-main.log | grep "database system is ready to accept connections"; do
    echo "Wait for connection"
    sleep 5
done;

echo "Citus is up and running"

tail -f /var/log/postgresql/postgresql-10-main.log
