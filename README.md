# Klara API

[![pipeline status](https://gitlab.com/klara-org/klara-api/badges/master/pipeline.svg)](https://gitlab.com/klara-org/klara-api/commits/master)

[![coverage report](https://gitlab.com/klara-org/klara-api/badges/master/coverage.svg?job=test)](https://gitlab.com/klara-org/klara-api/commits/master)

* Ruby version: 2.5.3
* Rails version: 5.2.1
* Redis version: 4.0.11
* Postgres version: 9.6.0

## Introduction

Klara API is an application developed using Ruby on Rails API. It is a Free Software Project, developed initially to PI2 subject of UnB - University of Brasília - Engeneering Campus (FGA). The Klara API is responsible for storing and provisioning all application data. There will be sent some data relating to the water properties(_in_ and _out_) in order to keep a robust database concerning the water quality of certain region. Besides that, it provisions the needed data for the
frontend, so that it groups the data and turn them into valuable information, graphs and statistics.

## Development setup

Development environment uses containers architecture through _Docker_ and _Docker Compose_.

### Docker Usage
* Download and install Docker CE at the [official site](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/#install-from-a-package).

* Download and install Docker Compose at the [official site](https://docs.docker.com/compose/install/#master-builds).

#### Clone the repository and enter it
```
git clone https://gitlab.com/klara-org/klara-api.git && cd klara-api/
```

#### Lift your environment
```
docker-compose up
```

### Useful commands
#### How to download a docker image
```
docker pull imageYouWant
```

#### Listing local images
```
docker images
```

#### Deleting images
```
docker rmi -f imageId
```

#### Listing running containers
```
docker ps
```

#### Removing containers
```
docker rm [-f] containerNameOrId
```

#### Executing commands from outside the container
```
docker exec <container-name> <desired-command>
```
Example:
```
docker exec klara-api rails generate model User name:string
```

## License

[MIT](https://gitlab.com/klara-org/klara-api/blob/master/LICENSE)

Copyright (c) 2018 Klara Organization
