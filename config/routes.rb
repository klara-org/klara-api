# frozen_string_literal: true

Rails.application.routes.draw do
  resources :regions
  resources :equipments
  resources :users
  resources :samples

  get "samples_state", to: "samples#show_complete_sample"

  post "login", to: "users#login"

  post "authenticate_equipment", to: "equipments#authenticate_equipment"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
