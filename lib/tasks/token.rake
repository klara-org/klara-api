namespace :token do
  namespace :keys do
    FILE_NAME = "env.yml"
    task :generate do
      unless File.file?(FILE_NAME)
        rsa_private = OpenSSL::PKey::RSA.generate 2048
        File.write(FILE_NAME, {
          "private_key" => rsa_private,
          "public_key" => rsa_private.public_key,
        }.to_yaml)
        puts "Your key was created!"
      else
        puts "The RSA Keys already exists"
      end
    end
    task :replace_token do
      rsa_private = OpenSSL::PKey::RSA.generate 2048
      File.write(FILE_NAME, {
        "private_key" => rsa_private,
        "public_key" => rsa_private.public_key,
      }.to_yaml)
      puts "Your key was replaced!"
    end
  end
end
