# frozen_string_literal: true

class CreateInitialTables < ActiveRecord::Migration[5.2]
  def change
    create_table :regions, partition_key: :id do |t|
      t.string :name
      t.string :initials

      t.timestamps
    end

    create_table :equipment, partition_key: :region_id do |t|
      t.references :region, null: false

      t.string :serial_number
      t.date :setup_date
      t.jsonb :geolocation
      t.string :setup_code
      t.string :password_digest

      t.timestamps
    end

    create_table :samples, partition_key: :region_id do |t|
      t.references :region, null: false
      t.references :equipment, null: false

      t.float :temperature
      t.float :ph
      t.float :turbidity
      t.float :tds
      t.datetime :collection_date

      t.timestamps
    end

    create_distributed_table :regions, :id
    create_distributed_table :equipment, :region_id
    create_distributed_table :samples, :region_id
  end
end
