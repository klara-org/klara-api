class AddCitusExtension < ActiveRecord::Migration[5.2]
  def change
    enable_extension "citus"
  end
end
