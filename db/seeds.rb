# frozen_string_literal: true

# require "securerandom"

previous_region_number = Region.all.count
regions = [
  ["Centro Oeste", "CO"],
  ["Nordeste", "NE"],
  ["Norte", "N"],
  ["Sudeste", "SE"],
  ["Sul", "S"]
]

(0..4).to_a.each do |index|
  Region.find_or_create_by(name: regions[index].first, initials: regions[index].second)
end
puts "[SEED] Successfully created #{Region.all.count - previous_region_number} regions!"

regions = Region.last(5)
previous_equipment_number = Equipment.all.count
(0..4).to_a.each do |index|
  Equipment.find_or_create_by(serial_number: "dummy-serial-number-#{index}",
                              setup_date: DateTime.new,
                              geolocation: "{\"place_id\":\"137230419\",\"licence\":\"Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright\",\"osm_type\":\"way\",\"osm_id\":\"260743866\",\"lat\":\"-22.90470495\",\"lon\":\"-43.209282658979\",\"display_name\":\"GRES Unidos da Tijuca, Avenida Francisco Bicalho, Santo Cristo, Zona Central do Rio de Janeiro, Rio de Janeiro, Região Metropolitana do Rio de Janeiro, Rio de Janeiro, Southeast Region, 20220-310, Brazil\",\"address\":{\"attraction\":\"GRES Unidos da Tijuca\",\"road\":\"Avenida Francisco Bicalho\",\"suburb\":\"Santo Cristo\",\"city_district\":\"Zona Central do Rio de Janeiro\",\"city\":\"Rio de Janeiro\",\"county\":\"Rio de Janeiro\",\"state_district\":\"Região Metropolitana do Rio de Janeiro\",\"state\":\"Rio de Janeiro\",\"postcode\":\"20220-310\",\"country\":\"Brazil\",\"country_code\":\"br\",\"region\":\"Sudeste\",\"uf\":\"RJ\"},\"boundingbox\":[\"-22.904883\",\"-22.9042832\",\"-43.2094612\",\"-43.2088182\"]}",
                              region_id: regions[index].id,
                              setup_code: SecureRandom.hex(32) + SecureRandom.hex(4),
                              password_digest: "dummy-pass-digest-#{index}")
end
puts "[SEED] Successfully created #{Equipment.all.count - previous_equipment_number} equipments!"

previous_sample_number = Sample.all.count
(0..24).to_a.each do |index|
  equipment = Equipment.all.to_a[index % 5]

  Sample.find_or_create_by(temperature: index,
                           ph: index,
                           turbidity: index,
                           tds: index,
                           region_id: equipment.region.id,
                           equipment_id: equipment.id,
                           collection_date: DateTime.now)
end
puts "[SEED] Successfully created #{Sample.all.count - previous_sample_number} samples!"
