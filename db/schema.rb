# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_10_05_234644) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "citus"
  enable_extension "plpgsql"

  create_table "equipment", primary_key: ["id", "region_id"], force: :cascade do |t|
    t.bigserial "id", null: false
    t.bigint "region_id", null: false
    t.string "serial_number"
    t.date "setup_date"
    t.jsonb "geolocation"
    t.string "setup_code"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["region_id"], name: "index_equipment_on_region_id"
  end

  create_table "regions", force: :cascade do |t|
    t.string "name"
    t.string "initials"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "samples", primary_key: ["id", "region_id"], force: :cascade do |t|
    t.bigserial "id", null: false
    t.bigint "region_id", null: false
    t.bigint "equipment_id", null: false
    t.float "temperature"
    t.float "ph"
    t.float "turbidity"
    t.float "tds"
    t.datetime "collection_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["equipment_id"], name: "index_samples_on_equipment_id"
    t.index ["region_id"], name: "index_samples_on_region_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "username"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end
end
