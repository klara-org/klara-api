class CreateEquipmentsJsonCacheJob < ApplicationJob
  queue_as :default

  def perform(*args)
    equipments = Equipment.includes(:samples)
    Rails.cache.fetch(Equipment.cache_key(equipments)) do
      equipments.to_json(include: :samples)
    end
  end
end
