class CreateSamplesJsonCacheJob < ApplicationJob
  queue_as :default

  def perform(*args)
    samples = Sample.all
    Rails.cache.fetch(Sample.cache_key(samples)) do
      samples.to_json
    end
  end
end
