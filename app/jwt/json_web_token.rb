class JsonWebToken
  def self.encode(payload, exp = 24.hours.from_now)
    JWT.encode(payload, nil, "none")
  end

  def self.decode(token)
    body = JWT.decode(token, nil, false)[0]
    HashWithIndifferentAccess.new body
  rescue JWT::ExpiredSignature, JWT::VerificationError => e
    raise ExceptionHandler::ExpiredSignature, e.message
  rescue JWT::DecodeError, JWT::VerificationError => e
    raise ExceptionHandler::DecodeError, e.message
  end
end
