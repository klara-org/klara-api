# frozen_string_literal: true

class Sample < ApplicationRecord
  multi_tenant :region
  belongs_to :region
  belongs_to :equipment, touch: true

  validates_presence_of :collection_date, :region
  validate :has_at_least_one_indicator
  after_save :create_samples_json_cache
  # delegate :region, to: :equipment

  # TODO: Check all units
  UNITS_OF_MEASUREMENT = {
    ph: "pH",
    temperature: "Celsius",
    turbidity: "Turbidity",
    tds: "ppm"
  }

  def self.cache_key(samples)
    {
      serializer: "samples",
      stat_record: samples.maximum(:updated_at)
    }
  end

  # def location_changed?(new_region)
  #   new_region.id != self.region_id
  # end

  private

    def has_at_least_one_indicator
      indicators = ["temperature", "ph", "turbidity", "tds"]

      if indicators.all? { |indicator| self[indicator].blank? }
        errors.add(:sample, "must have at least one indicator")
      end
    end

    def create_samples_json_cache
      CreateSamplesJsonCacheJob.perform_later
    end
end
