class Equipment < ApplicationRecord
  multi_tenant :region
  belongs_to :region
  has_many :samples

  validates_presence_of :password_digest, :serial_number, :setup_code
  validates :serial_number, uniqueness: true
  after_save :create_equipments_json_cache

  has_secure_password

  def self.cache_key(equipments)
    {
      serializer: "equipments",
      stat_record: equipments.maximum(:updated_at)
    }
  end

  def location
    JSON.parse(self.geolocation).with_indifferent_access unless self.geolocation.nil?
  end

  def address
    self.geolocation.nil? ? {} : location[:address]
  end

  def location_changed?(new_location)
    new_location["address"]["city"] != self.address[:city]
  end

  private

    def create_equipments_json_cache
      CreateEquipmentsJsonCacheJob.perform_later
    end
end
