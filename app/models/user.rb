class User < ApplicationRecord
  validates_presence_of :password_digest, :username
  validates :username, uniqueness: true

  has_secure_password
end
