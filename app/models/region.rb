class Region < ApplicationRecord
  multi_tenant :region
  has_many :equipment
  has_many :samples

  validates_presence_of :name
  validates :name, uniqueness: true
end
