module AuthorizationHelper
  def authenticate_request
    if request.headers["Authorization"].present?
      @token = request.headers["Authorization"].split(" ").last
      get_current_user
      render json: { error: "Not Authorized! Authenticable entity not found." }, status: 401 unless @current_user
    else
      render json: { error: "Not Authorized! 'Authorization' header field not found." }, status: 401
    end
  end

  def get_current_user
    entity_id = JsonWebToken.decode(@token)

    case entity_id[:type]
    when "user"
      @current_user ||= User.find_by_id(entity_id[:authenticable_id])
    when "equipment"
      @current_user ||= Equipment.find_by_id(entity_id[:authenticable_id])
    end
  end

  def decrypt_password(key, salt, password_digest)
    ascii_8bit_password = Base64.decode64(password_digest.encode("ascii-8bit"))

    decrypter = OpenSSL::Cipher.new "AES-128-CBC"
    decrypter.decrypt
    decrypter.pkcs5_keyivgen key, salt

    password = decrypter.update ascii_8bit_password
    password << decrypter.final
  end

  def get_password(setup_code, password)
    unless password.nil?
      setup_data = {
        key: get_string_chars(32, 32, setup_code),
        salt: get_string_chars(64, 8, setup_code),
      }
      return decrypt_password(setup_data[:key], setup_data[:salt], password)
    else
      return nil
    end
  end

  def get_string_chars(start, size, str)
    str.chars[start..(start + size - 1)].join
  end
end
