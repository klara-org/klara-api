class Authentication
  def initialize(credentials)
    @username = credentials[:username]
    @password = credentials[:password]
    @auth_type = credentials[:type]
    @authenticable = get_authenticable
  end

  def authenticate
    true if @authenticable && @authenticable.authenticate(@password)
  end

  def generate_token
    JsonWebToken.encode(authenticable_id: @authenticable.id, type: @auth_type)
  end

  def get_authenticable
    case @auth_type
    when "user"
      return User.find_by(username: @username)
    when "equipment"
      return Equipment.find_by(serial_number: @username)
    end
  end
end
