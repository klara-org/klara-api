# frozen_string_literal: true

class ApplicationController < ActionController::API
  include ExceptionHandler
  include AuthorizationHelper
  include Geocoding

  set_current_tenant_through_filter
  before_action :set_current_region
  before_action :authenticate_request
  before_action :set_cors_headers

  private

    def set_geolocation_result
      @result = search(params[:latitude], params[:longitude])
    end

    def current_region
      nil
    end

    def set_current_region
      set_current_tenant(current_region)
    end

    def set_cors_headers
      response.set_header "Access-Control-Allow-Origin", origin
    end

    def origin
      request.headers["Origin"] || "*"
    end
end
