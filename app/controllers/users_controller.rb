class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]
  skip_before_action :authenticate_request, only: [:create, :login]

  # GET /users/1
  def show
    @user = User.find(params[:id])
    if @user.id == @current_user.id
      render json: { id: @user.id, name: @user.username }
    else
      render json: { error: "Not Authorized" }, status: 401
    end
  end

  # POST /users
  def create
    @user = User.new(user_params)

    if @user.save
      auth_object = Authentication.new(get_credentials)
      result = { token: auth_object.generate_token, username: @user.username, user_id: @user.id }
      render json: result, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def login
    user = User.find_by(username: params[:user][:username])
    auth_object = Authentication.new(get_credentials)
    if auth_object.authenticate
      render json: {
        message: "Login successful!", token: auth_object.generate_token, id: user.id
      }, status: :ok
    else
      render json: {
        message: "Incorrect username/password combination"
      }, status: :unauthorized
    end
  end

  # DELETE /users/1
  def destroy
    @user.destroy
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    def get_credentials
      credentials = user_params
      credentials[:type] = "user"
      return credentials
    end

    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:user).permit(:username, :password)
    end
end
