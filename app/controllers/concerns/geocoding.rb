module Geocoder::Result
  class Nominatim < Base
    def region
      @data["address"]["region"]
    end

    def uf
      @data["address"]["uf"]
    end

    def is_valid?
      @data["error"].blank?
    end
  end
end

module Geocoding
  extend ActiveSupport::Concern

  BRAZIL_REGIONS_INITIALS = {
    "Centro Oeste": "CO",
    "Nordeste": "NE",
    "Norte": "N",
    "Sudeste": "SE",
    "Sul": "S"
  }.with_indifferent_access

  REGIONS_OF_BRAZIL = {
    "Centro Oeste": ["Distrito Federal", "Federal District", "Goiás", "Mato Grosso", "Mato Grosso do Sul"],
    "Nordeste": [
      "Alagoas", "Bahia", "Ceará", "Maranhão", "Paraíba", "Pernambuco",
      "Piauí", "Rio Grande do Norte", "Sergipe"
    ],
    "Norte": ["Acre", "Amapá", "Amazonas", "Pará", "Rondônia", "Roraima", "Tocantins"],
    "Sudeste": ["Espírito Santo", "Minas Gerais", "São Paulo", "Rio de Janeiro"],
    "Sul": ["Paraná", "Rio Grande do Sul", "Santa Catarina"]
  }.with_indifferent_access

  BRAZIL_UFS = {
    "Acre": "AC",
    "Alagoas": "AL",
    "Amapá": "AP",
    "Amazonas": "AM",
    "Bahia": "BA",
    "Ceará": "CE",
    "Distrito Federal": "DF",
    "Federal District": "DF",
    "Espírito Santo": "ES",
    "Goiás": "GO",
    "Maranhão": "MA",
    "Mato Grosso": "MT",
    "Mato Grosso do Sul": "MS",
    "Minas Gerais": "MG",
    "Pará": "PA",
    "Paraíba": "PB",
    "Paraná": "PR",
    "Pernambuco": "PE",
    "Piauí": "PI",
    "Rio de Janeiro": "RJ",
    "Rio Grande do Norte": "RN",
    "Rio Grande do Sul": "RS",
    "Rondônia": "RO",
    "Roraima": "RR",
    "Santa Catarina": "SC",
    "São Paulo": "SP",
    "Sergipe": "SE",
    "Tocantins": "TO"
  }.with_indifferent_access

  def get_region_of_state(state)
    REGIONS_OF_BRAZIL.detect { |region, region_states| return region if region_states.include? state }
  end

  def search(latitude, longitude)
    result = Geocoder.search([latitude, longitude]).first

    if result && result.is_valid?
      result.data["address"]["region"] = get_region_of_state(result.state)
      result.data["address"]["uf"] = BRAZIL_UFS[result.state]
      return result
    else
      return nil
    end
  end
end
