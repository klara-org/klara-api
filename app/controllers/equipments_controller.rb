class EquipmentsController < ApplicationController
  before_action :set_equipment, only: [:show, :update, :destroy]
  skip_before_action :authenticate_request, only: [:index, :show, :create, :authenticate_equipment]

  # GET /equipments
  def index
    equipments = Equipment.includes(:samples)
    @equipments = Rails.cache.fetch("equipments") do
      equipments.to_json(include: :samples)
    end

    render json: @equipments
  end

  # GET /equipment/1
  def show
    render json: @equipment
  end

  # POST /equipments
  def create
    @equipment = Equipment.new(equipment_data)

    if @equipment.region_id.nil?
      @equipment.region_id = Region.find_or_create_by(name: "Centro Oeste", initials: "CO").id
    end

    if @equipment.save
      auth_object = Authentication.new(get_credentials)
      result = {
        token: auth_object.generate_token,
        serial_number: @equipment.serial_number,
        setup_date: @equipment.setup_date,
        geolocation: @equipment.geolocation,
        setup_code: @equipment.setup_code,
        equipment_id: @equipment.id
      }

      render json: result, status: :created, location: @equipment
    else
      render json: @equipment.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /equipment/1
  def update
    if @equipment.update(equipment_params)
      render json: @equipment
    else
      render json: @equipment.errors, status: :unprocessable_entity
    end
  end

  # DELETE /equipment/1
  def destroy
    @equipment.destroy
  end

  def authenticate_equipment
    auth_object = Authentication.new(get_credentials)
    if auth_object.authenticate
      render json: {
        message: "Successfully authenticated!", token: auth_object.generate_token
      }, status: :ok
    else
      render json: {
        message: "Invalid credentials"
      }, status: :unauthorized
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_equipment
      @equipment = Equipment.find(params[:id])
    end

    def get_credentials
      data = equipment_data
      credentials = Hash.new
      credentials[:username] = data[:serial_number]
      credentials[:password] = data[:password]
      credentials[:type] = "equipment"
      return credentials
    end

    def equipment_data
      data = equipment_params
      data[:password] = get_password(equipment_params[:setup_code], equipment_params[:password])
      return data
    end

    # Only allow a trusted parameter "white list" through.
    def equipment_params
      params.require(:equipment).permit(:setup_code, :serial_number, :password, :setup_date, :geolocation, :region_id)
    end
end
