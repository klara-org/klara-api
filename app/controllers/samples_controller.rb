# frozen_string_literal: true

class SamplesController < ApplicationController
  include Geocoding

  skip_before_action :authenticate_request, only: [:index, :show, :show_complete_sample]
  before_action :set_sample, only: [:show, :update, :destroy]
  after_action :update_equipment_geolocation, only: [:create, :update]

  # GET /samples
  def index
    samples = Sample.all
    @samples = Rails.cache.fetch(samples) do
      samples
    end

    render json: @samples
  end

  # GET /samples/1
  def show
    render json: @sample
  end

  def show_complete_sample
    samples = Rails.cache.fetch(Sample.all) do
      Sample.all
    end
    samples_with_state = []
    samples.each do |sample|
      lat_long = []
      lat_long << (sample.equipment.location[:lat]).to_f
      lat_long << (sample.equipment.location[:lon]).to_f
      temporary_sample = Hash.new
      temporary_sample[:ph] = sample.ph
      temporary_sample[:temperatura] = sample.temperature
      temporary_sample[:tds] = sample.tds
      temporary_sample[:turbidez] = sample.turbidity
      temporary_sample[:state] = sample.equipment.address[:uf]
      temporary_sample[:iqa] = iqa_calculate (sample)
      temporary_sample[:coordenadas] = lat_long
      samples_with_state.push(temporary_sample)
    end
    render json: samples_with_state
  end

  def iqa_calculate(sample)
    water_quality = 0

    if (sample.ph >= 6 && sample.ph <= 9)
      water_quality += 1
    elsif ((sample.ph >= 0 && sample.ph <= 2) || (sample.ph >= 12 && sample.ph <= 14))
      water_quality += 2

    elsif ((sample.ph > 2 && sample.ph <= 3) || (sample.ph >= 11 && sample.ph < 12))
      water_quality += 3

    elsif ((sample.ph > 3 && sample.ph <= 4) || (sample.ph >= 10 && sample.ph < 11))
      water_quality += 4

    elsif ((sample.ph > 4 && sample.ph < 6) || (sample.ph > 9 && sample.ph < 10))
      water_quality += 5
    end

    if (sample.tds <= 500)
      water_quality += 1

    elsif (sample.tds > 500 && sample.tds <= 550)
      water_quality += 2

    elsif (sample.tds > 550 && sample.tds <= 600)
      water_quality += 3

    elsif (sample.tds > 650 && sample.tds <= 700)
      water_quality += 4

    elsif (sample.tds > 750)
      water_quality += 5
    end

    if (sample.turbidity <= 100)
      water_quality += 1

    elsif (sample.turbidity > 100 && sample.turbidity <= 150)
      water_quality += 2

    elsif (sample.turbidity > 150 && sample.turbidity <= 200)
      water_quality += 3

    elsif (sample.turbidity > 200 && sample.turbidity <= 250)
      water_quality += 4

    elsif (sample.turbidity > 250)
      water_quality += 5
    end

    (water_quality / 3).ceil
  end

  # POST /samples
  def create
    if @current_user.instance_of? User
      equipment = Equipment.find_by(serial_number: params[:serial_number])
    else
      equipment = @current_user
    end

    sample_data = sample_params
    sample_data[:equipment] = equipment

    @sample = Sample.new(get_sample_data)

    set_sample_region

    if @sample.save
      render json: @sample, status: :created, location: @sample
    else
      render json: @sample.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /samples/1
  def update
    set_sample_region

    if @sample.update(get_sample_data)
      render json: @sample
    else
      render json: @sample.errors, status: :unprocessable_entity
    end
  end

  # DELETE /samples/1
  def destroy
    @sample.destroy
  end

  private

    def find_region
      region_name = @result.region
      region_initials = Geocoding::BRAZIL_REGIONS_INITIALS[region_name]
      searched_region = Region.find_or_create_by(name: region_name, initials: region_initials)

      searched_region
    end

    def set_sample_region
      set_geolocation_result
      unless @result.nil?

        @sample.region = find_region
      end
    end

    def update_equipment_geolocation
      set_geolocation_result
      equipment = get_equipment
      unless @result.nil?
        unless find_region.nil?
          equipment.update!(geolocation: @result.data.to_json) if equipment.location_changed?(@result.data)
        end
      end
    end

    def get_equipment
      if @current_user.instance_of? User
        return Equipment.find_by(serial_number: params[:serial_number])
      else
        return @current_user
      end
    end

    def get_sample_data
      sample_data = sample_params
      sample_data[:equipment] = get_equipment
      return sample_data
    end

    def set_sample
      @sample = Sample.find(params[:id])
    end

    def sample_params
      params.require(:sample).permit(:temperature, :ph, :turbidity, :tds, :collection_date)
    end

  # def set_geolocation_result
  #   @result = search(params[:latitude], params[:longitude])
  # end

  # FIXME: Is this really needed?
  # def geolocation_params
  #   params.require(:equipment).permit(:geolocation)
  # end
end
