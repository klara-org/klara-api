FROM ruby:2.5.3-slim

RUN apt-get update -qq && \
    apt-get install -y nodejs \
                       postgresql \
                       postgresql-client \
                       postgresql-contrib \
                       build-essential \
                       libpq-dev \
                       curl \
                       git \
                       ruby-dev \
                       yarn

COPY . /klara-api

WORKDIR /klara-api

CMD ["sh","script/start.sh"]
