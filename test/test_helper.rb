# frozen_string_literal: true

require "simplecov"
SimpleCov.start do
  add_filter "vendor/"
  add_filter "app/jobs/"
  add_filter "config/"
  add_filter "test/"
end
SimpleCov.formatter = SimpleCov::Formatter::HTMLFormatter

ENV["RAILS_ENV"] ||= "test"
require_relative "../config/environment"
require "rails/test_help"
require "mocha/minitest"

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
end
