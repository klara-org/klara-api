require "test_helper"

class EquipmentControllerTest < ActionDispatch::IntegrationTest
  setup do
    @equipment = equipments(:equipment_one)
    @equipment_token = JsonWebToken.encode(authenticable_id: @equipment.id, type: "equipment")
    @equipment2 = equipments(:equipment_two)
    @new_equipment = Equipment.new(
      serial_number: "26345577884238858860753298162624",
      setup_code: "263455778842388588607532981626243974180164606208493907818496308177894154",
      password_digest: "M4IuCOBOrXqlM1qIlCQ5WS8eDVw9tK8lR6I7iqYXwi4=\n",
      region_id: @equipment.region.id
    )
  end

  test "should get index" do
    get equipments_url, as: :json
    assert_response :success
  end

  test "should create equipment" do
    assert_difference("Equipment.count") do
      post equipments_url, params: {
        equipment: {
          serial_number: @new_equipment.serial_number,
          setup_code: @new_equipment.setup_code,
          password: @new_equipment.password_digest,
          region_id: @equipment.region.id
        }
      }, as: :json
    end

    assert_response 201
  end

  test "should not create a equipment with missing parameters" do
    post equipments_url, params: {
      equipment: {
        serial_number: @equipment.serial_number
      }
    }, as: :json
    assert_response :unprocessable_entity

    post equipments_url, params: {
      equipment: {
        setup_code: @equipment.setup_code
      }
    }, as: :json
    assert_response :unprocessable_entity

    post equipments_url, params: {
      equipment: {
        password: @equipment.password
      }
    }, as: :json
    assert_response :unprocessable_entity
  end

  test "should show equipment" do
    get equipment_url(@equipment), as: :json
    assert_response :success
  end

  test "should update equipment" do
    patch equipment_url(@equipment),
          headers: { "Authorization" => @equipment_token },
          params: {
            equipment: {
              serial_number: @new_equipment.serial_number,
              setup_code: @new_equipment.setup_code
            }
          }, as: :json
    assert_response 200
  end

  test "should not update an equipment serial_number to an existent one" do
    patch equipment_url(@equipment),
          headers: { "Authorization" => @equipment_token },
          params: {
            equipment: {
              serial_number: @equipment2.serial_number
            }
          }, as: :json
    assert_response :unprocessable_entity
  end

  test "should authenticate an equipment with correct credentials" do
    post equipments_url, params: {
      equipment: {
        serial_number: @new_equipment.serial_number,
        setup_code: @new_equipment.setup_code,
        password: @new_equipment.password_digest,
        region_id: @equipment.region.id
      }
    }, as: :json

    new_equipment_id = JSON.parse(response.body)["equipment_id"]

    post authenticate_equipment_url, params: {
      equipment: {
        serial_number: @new_equipment.serial_number,
        setup_code: @new_equipment.setup_code,
        password: @new_equipment.password_digest
      }
    }, as: :json
    assert_response :ok

    token = JSON.parse(response.body)["token"]
    assert_equal new_equipment_id, JsonWebToken.decode(token)["authenticable_id"]
  end

  test "should not authenticate an equipment with incorrect credentials" do
    auth_equipment = Equipment.create(
      setup_code: @new_equipment.setup_code,
      serial_number: @new_equipment.serial_number,
      password: @new_equipment.password_digest
    )
    post authenticate_equipment_url, params: {
      equipment: {
        serial_number: auth_equipment.serial_number,
        setup_code: auth_equipment.setup_code,
        password: "rBhqmeuLssmUaOH2URh37gzrLNcyT0yoRr/Nj/oYXk8=\n"
      }
    }, as: :json
    assert_response :unauthorized
  end

  test "should destroy equipment" do
    assert_difference("Equipment.count", -1) do
      delete equipment_url(@equipment), headers: { "Authorization" => @equipment_token }, as: :json
    end

    assert_response 204
  end
end
