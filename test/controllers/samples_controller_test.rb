# frozen_string_literal: true

require "test_helper"

class SamplesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @sample = samples(:sample_one)
    @user = users(:one)
    @user_token = JsonWebToken.encode(authenticable_id: @user.id, type: "user")
    @equipment = equipments(:equipment_one)
    @equipment_token = JsonWebToken.encode(authenticable_id: @equipment.id, type: "equipment")
    @manaus_coordinates = {
      lat: -3.1071,
      long: -60.0261
    }
  end

  test "user and equipment should get index" do
    get samples_url, headers: { "Authorization" => @user_token }, as: :json
    assert_response :success
    get samples_url, headers: { "Authorization" => @equipment_token }, as: :json
    assert_response :success
  end

  test "user and equipment should create sample" do
    assert_difference("Sample.count") do
      post samples_url, headers: { "Authorization" => @user_token }, params: {
        collection_date: @sample.collection_date,
        ph: @sample.ph,
        tds: @sample.tds,
        temperature: @sample.temperature,
        turbidity: @sample.turbidity,
        serial_number: @equipment.serial_number,
        latitude: -23.5489,
        longitude: -46.6388
      }, as: :json
    end

    assert_difference("Sample.count") do
      post samples_url, headers: { "Authorization" => @equipment_token }, params: {
        collection_date: @sample.collection_date,
        ph: @sample.ph,
        tds: @sample.tds,
        temperature: @sample.temperature,
        turbidity: @sample.turbidity,
        latitude: -23.5489,
        longitude: -46.6388
      }, as: :json
    end

    assert_response 201
  end

  test "should show complete sample" do
    get "/samples_state"

    assert response.parsed_body[0]["ph"] == 6
    assert response.parsed_body[0]["turbidez"] == 100
    assert response.parsed_body[0]["temperatura"] == 1.5
    assert response.parsed_body[0]["tds"] == 500
    assert response.parsed_body[0]["iqa"] == 1
    assert response.parsed_body[0]["state"] == "SP"
    assert response.parsed_body[0]["coordenadas"] == [-23.500335054342, -46.5895760688661]

    assert_response :success
  end

  test "should save sample region" do
    assert_difference("Sample.count") do
      post samples_url, headers: { "Authorization" => @equipment_token }, params: {
        collection_date: @sample.collection_date,
        ph: @sample.ph,
        tds: @sample.tds,
        temperature: @sample.temperature,
        turbidity: @sample.turbidity,
        latitude: @manaus_coordinates[:lat],
        longitude: @manaus_coordinates[:long]
      }, as: :json
    end

    assert_response 201
    assert_equal "Norte", Sample.last.region.name
  end

  test "should not create sample with invalid params" do
    assert_no_difference("Sample.count") do
      post samples_url, headers: { "Authorization" => @user_token }, params: {
        collection_date: @sample.collection_date,
        ph: "",
        tds: "",
        temperature: "",
        turbidity: "",
        serial_number: @equipment.serial_number,
        latitude: -23.5489,
        longitude: -46.6388
      }, as: :json
    end

    assert_response :unprocessable_entity
  end

  test "should show sample to user and equipment" do
    get sample_url(@sample), headers: { "Authorization" => @user_token }, as: :json
    assert_response :success
    get sample_url(@sample), headers: { "Authorization" => @equipment_token }, as: :json
    assert_response :success
  end

  test "user and equipment should update sample" do
    patch sample_url(@sample), headers: { "Authorization" => @user_token }, params: {
      collection_date: @sample.collection_date,
      ph: @sample.ph,
      tds: @sample.tds,
      temperature: @sample.temperature,
      turbidity: @sample.turbidity,
      serial_number: @equipment.serial_number,
      latitude: -23.5489,
      longitude: -46.6388
    }, as: :json
    assert_response 200

    patch sample_url(@sample), headers: { "Authorization" => @equipment_token }, params: {
      collection_date: @sample.collection_date,
      ph: @sample.ph,
      tds: @sample.tds,
      temperature: @sample.temperature,
      turbidity: @sample.turbidity,
      latitude: -23.5489,
      longitude: -46.6388
    }, as: :json
    assert_response 200
  end

  test "should not update sample with invalid params" do
    patch sample_url(@sample), headers: { "Authorization" => @user_token }, params: {
      collection_date: "",
      ph: @sample.ph,
      tds: @sample.tds,
      temperature: @sample.temperature,
      turbidity: @sample.turbidity,
      serial_number: @equipment.serial_number,
      latitude: -23.5489,
      longitude: -46.6388
    }, as: :json
    assert_response :unprocessable_entity
  end

  test "user should destroy sample" do
    assert_difference("Sample.count", -1) do
      delete sample_url(@sample), headers: { "Authorization" => @user_token }, as: :json
    end

    assert_response 204
  end

  test "sample should update its equipment location" do
    equipment = equipments(:equipment_one)
    old_location = equipment.location

    assert_difference("Sample.count") do
      post samples_url, headers: { "Authorization" => @equipment_token }, params: {
        collection_date: @sample.collection_date,
        ph: @sample.ph,
        tds: @sample.tds,
        temperature: @sample.temperature,
        turbidity: @sample.turbidity,
        serial_number: @equipment.serial_number,
        latitude: @manaus_coordinates[:lat],
        longitude: @manaus_coordinates[:long]
      }, as: :json
    end
    equipment.reload

    assert_not_equal equipment.location, old_location
  end

  test "sample should not update its equipment location if geolocation is nil" do
    equipment = equipments(:equipment_one)
    old_location = equipment.location
    old_region = equipment.region.name

    assert_no_difference("Sample.count") do
      post samples_url, headers: { "Authorization" => @equipment_token }, params: {
        collection_date: @sample.collection_date,
        ph: @sample.ph,
        tds: @sample.tds,
        temperature: @sample.temperature,
        turbidity: @sample.turbidity,
        serial_number: @equipment.serial_number,
        latitude: 0.0000,
        longitude: 0.0000
      }, as: :json
    end
    equipment.reload

    assert_equal old_location, equipment.location
    assert_equal old_region, equipment.region.name
  end

  test "sample should not update its equipment location if geolocation didn't change" do
    equipment = equipments(:equipment_one)
    old_location = equipment.location
    old_region = equipment.region.name

    assert_difference("Sample.count") do
      post samples_url, headers: { "Authorization" => @equipment_token }, params: {
        collection_date: @sample.collection_date,
        ph: @sample.ph,
        tds: @sample.tds,
        temperature: @sample.temperature,
        turbidity: @sample.turbidity,
        serial_number: @equipment.serial_number,
        latitude: -23.500335054342,
        longitude: -46.5895760688661
      }, as: :json
    end
    equipment.reload

    assert_equal old_location, equipment.location
    assert_equal old_region, equipment.region.name
  end
end
