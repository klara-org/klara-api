require "test_helper"

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
    @user_token = JsonWebToken.encode(authenticable_id: @user.id, type: "user")
    @user2 = users(:two)
    @new_user = User.new(username: "John Doe", password: "applejuice")
  end

  test "should create user" do
    assert_difference("User.count") do
      post users_url, params: {
        user: {
          username: @new_user.username,
          password: @new_user.password
        }
      }, as: :json
    end
    assert_response 201
  end

  test "should not create a user with missing parameters" do
    post users_url, params: { user: { username: @user.username } }, as: :json
    assert_response :unprocessable_entity

    post users_url, params: { user: { password: @user2.password } }, as: :json
    assert_response :unprocessable_entity
  end

  test "should show user" do
    get user_url(@user), headers: { "Authorization" => @user_token }, as: :json
    assert_response :success
  end

  test "should not show user without authorization token" do
    get user_url(@user), as: :json
    assert_response :unauthorized
  end

  test "should not show user with invalid token" do
    get user_url(@user), headers: { "Authorization" => "invalid-token" }, as: :json

    assert_response :unauthorized
    assert_match /Access denied!. Invalid token supplied./, response.body
  end

  test "should not show user with expired token" do
    JWT.stubs(:decode).raises(JWT::ExpiredSignature)

    get user_url(@user), headers: { "Authorization" => "a-expired-token" }, as: :json

    assert_response :unauthorized
    assert_match /Access denied!. Token has expired./, response.body
  end

  test "should not show information to other user" do
    get user_url(@user2), headers: { "Authorization" => @user_token }, as: :json
    assert_response :unauthorized
  end

  test "should update user" do
    patch user_url(@user), headers: { "Authorization" => @user_token }, params: {
      user: {
        username: @user.username,
        password_digest: @user.password_digest
      }
    }, as: :json
    assert_response 200
  end

  test "should not change a username to an existent one" do
    patch user_url(@user), headers: { "Authorization" => @user_token }, params: {
      user: {
        username: @user2.username
      }
    }, as: :json
    assert_response :unprocessable_entity
  end

  test "should log a user with correct credentials in" do
    login_user = User.create(username: @new_user.username, password: @new_user.password)
    post login_url, params: {
      user: {
        username: login_user.username,
        password: login_user.password
      }
    }, as: :json
    assert_response :ok

    token = JSON.parse(response.body)["token"]
    assert_equal login_user.id, JsonWebToken.decode(token)["authenticable_id"]
  end

  test "should not log a user with incorrect credentials in" do
    login_user = User.create(username: @new_user.username, password: @new_user.password)
    post login_url, params: {
      user: {
        username: login_user.username,
        password: "maplesyrup"
      }
    }, as: :json
    assert_response :unauthorized
  end

  test "should destroy user" do
    assert_difference("User.count", -1) do
      delete user_url(@user), headers: { "Authorization" => @user_token }, as: :json
    end
    assert_response 204
  end
end
