require "test_helper"

class RegionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @region = regions(:region_one)
    @another_region = regions(:region_two)
  end

  test "should get index" do
    get regions_url, as: :json
    assert_response :success
  end

  test "should create region" do
    assert_difference("Region.count") do
      post regions_url, params: { region: { initials: "AR", name: "Another Region" } }, as: :json
    end

    assert_response 201
  end

  test "should not create invalid region" do
    post regions_url, params: { region: { initials: "AR", name: nil } }, as: :json
    assert_response :unprocessable_entity

    post regions_url, params: { region: { initials: "AR", name: @another_region.name } }, as: :json
    assert_response :unprocessable_entity
  end

  test "should show region" do
    get region_url(@region), as: :json
    assert_response :success
  end

  test "should not update invalid region" do
    patch region_url(@region), params: { region: { initials: "LI", name: nil } }, as: :json
    assert_response :unprocessable_entity

    patch region_url(@region), params: { region: { name: @another_region.name } }, as: :json
    assert_response :unprocessable_entity
  end

  test "should update region" do
    patch region_url(@region), params: { region: { initials: @region.initials, name: @region.name } }, as: :json
    assert_response 200
  end

  test "should destroy region" do
    assert_difference("Region.count", -1) do
      delete region_url(@region), as: :json
    end

    assert_response 204
  end
end
