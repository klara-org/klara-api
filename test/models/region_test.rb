require "test_helper"

class RegionTest < ActiveSupport::TestCase
  setup do
    @region = regions(:region_one)
    @another_region = regions(:region_two)
  end

  test "should save valid region" do
    assert @region.save!
    assert @another_region.save!
  end

  test "should not save invalid region" do
    invalid_region = Region.new(name: nil, initials: nil)
    another_invalid_region = Region.new(name: nil, initials: "PB")
    refute invalid_region.save
    refute another_invalid_region.save
  end
end
