require "test_helper"

class UserTest < ActiveSupport::TestCase
  setup do
    @user = users(:one)
  end

  test "should save a valid user" do
    assert @user.save!
  end

  test "should not save a user without username" do
    wrong_user = User.new(password_digest: "applejuice")
    assert_raise do
      wrong_user.save!
    end
    error_message = { username: ["can't be blank"] }
    assert_equal error_message, wrong_user.errors.messages
  end

  test "should not save a user without password_digest" do
    wrong_user = User.new(username: "John Doe")
    assert_raise do
      wrong_user.save!
    end
    error_message = { password_digest: ["can't be blank"], password: ["can't be blank"] }
    assert_equal error_message, wrong_user.errors.messages
  end

  test "should not save a user with a duplicated username" do
    wrong_user = User.new(username: @user.username, password_digest: "mapplesyrup")
    assert_raise do
      wrong_user.save!
    end
    error_message = { username: ["has already been taken"] }
    assert_equal error_message, wrong_user.errors.messages
  end
end
