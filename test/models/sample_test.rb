# frozen_string_literal: true

require "test_helper"

class SampleTest < ActiveSupport::TestCase
  setup do
    @sample = samples(:sample_one)
    @equipment = equipments(:equipment_one)
  end

  test "should save valid sample" do
    assert @sample.save!
  end

  test "should save sample with only one indicator" do
    sample = Sample.new(
      ph: 1.5,
      collection_date: DateTime.now,
      equipment: @equipment,
      region: @sample.region
    )
    assert sample.save!
  end

  test "should not save sample without collection date" do
    @sample.collection_date = ""
    refute @sample.save
  end

  test "should not save sample without any indicator" do
    invalid_sample = Sample.new(collection_date: DateTime.now, region: @sample.region)
    refute invalid_sample.save
  end

  # test "should change with different locations" do
  #   another_sample = samples(:sample_two)
  #   different_region = another_sample.region

  #   assert @sample.location_changed?(different_region)
  # end

  # test "should not change with equal locations" do
  #   refute @sample.location_changed?(@sample.region)
  # end
end
