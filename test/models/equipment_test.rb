require "test_helper"

class EquipmentTest < ActiveSupport::TestCase
  setup do
    @equipment = equipments(:equipment_one)
  end

  test "should save a valid equipment" do
    assert @equipment.save!
  end

  test "should not save a equipment without a serial_number" do
    wrong_equipment = Equipment.new(
      setup_date: "2018-10-09",
      region_id: @equipment.region_id,
      setup_code: "123456",
      password_digest: "applejuice"
    )
    assert_raise do
      wrong_equipment.save!
    end
    error_message = { serial_number: ["can't be blank"] }
    assert_equal error_message, wrong_equipment.errors.messages
  end

  test "should not save a equipment without a setup_code" do
    wrong_equipment = Equipment.new(
      serial_number: "123",
      region_id: @equipment.region_id,
      setup_date: "2018-10-09",
      password_digest: "applejuice"
    )
    assert_raise do
      wrong_equipment.save!
    end
    error_message = { setup_code: ["can't be blank"] }
    assert_equal error_message, wrong_equipment.errors.messages
  end

  test "should not save a equipment without a password_digest" do
    wrong_equipment = Equipment.new(
      serial_number: "123",
      region_id: @equipment.region_id,
      setup_date: "2018-10-09",
      setup_code: "123456"
    )
    assert_raise do
      wrong_equipment.save!
    end
    error_message = { password_digest: ["can't be blank"], password: ["can't be blank"] }
    assert_equal error_message, wrong_equipment.errors.messages
  end

  test "should not save a equipment with a duplicated serial_number" do
    wrong_equipment = Equipment.new(
      serial_number: @equipment.serial_number,
      region_id: @equipment.region_id,
      setup_code: "123456",
      password_digest: "mapplesyrup"
    )
    assert_raise do
      wrong_equipment.save!
    end
    error_message = { serial_number: ["has already been taken"] }
    assert_equal error_message, wrong_equipment.errors.messages
  end

  # FIXME: I'm not sure if this validation should exist
  # test "should not save a equipment without region" do
  #   wrong_equipment = Equipment.new(
  #     serial_number: "1234567890",
  #     region_id: @equipment.region_ide: "123456789",
  #     password_digest: "mapplesyrup"
  #   )

  #   refute wrong_equipment.save

  #   error_message = { region_id: @equipment.region_idt be blank"] }
  #   assert_equal error_message, wrong_equipment.errors.messages
  # end

  test "should get equipment location" do
    location = @equipment.location

    assert_equal "SP", location[:address][:uf]
    assert_equal "Sudeste", location[:address][:region]
    assert_equal "São Paulo", location[:address][:state]
  end

  test "should not get equipment location if geolocation is nil" do
    @equipment.geolocation = nil

    assert_nil @equipment.location
  end

  test "should get equipment address" do
    address = @equipment.address

    assert_equal "SP", address[:uf]
    assert_equal "Sudeste", address[:region]
    assert_equal "São Paulo", address[:state]
  end

  test "should not get equipment address if geolocation is nil" do
    @equipment.geolocation = nil

    assert_empty @equipment.address
  end

  test "should change with different locations" do
    another_equipment = equipments(:equipment_two)
    different_location = another_equipment.location

    assert @equipment.location_changed?(different_location)
  end

  test "should not change with equal locations" do
    refute @equipment.location_changed?(@equipment.location)
  end
end
