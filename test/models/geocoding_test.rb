# frozen_string_literal: true

require "test_helper"
include Geocoding

class GeocodingTest < ActiveSupport::TestCase
  test "should search" do
    latitude = -23.5489
    longitude = -46.6388
    result = Geocoding.search(latitude, longitude)

    assert_equal "SP", result.uf
    assert_equal "São Paulo", result.city
    assert_equal "São Paulo", result.state
    assert_equal "Sudeste", result.region
    assert_equal "Brazil", result.country
  end

  test "should have regions of Brazil on Geocoding module" do
    Geocoding::REGIONS_OF_BRAZIL.assert_valid_keys("Centro Oeste", "Nordeste", "Norte", "Sudeste", "Sul")
  end

  test "should have states of Brazil on Geocoding module" do
    assert_equal 3, Geocoding::REGIONS_OF_BRAZIL["Sul"].size
    assert_equal 7, Geocoding::REGIONS_OF_BRAZIL["Norte"].size
    assert_equal 4, Geocoding::REGIONS_OF_BRAZIL["Sudeste"].size
    assert_equal 9, Geocoding::REGIONS_OF_BRAZIL["Nordeste"].size

    # FIXME: There are TWO Federal Districts on the Hash, because one is in
    # portuguese and other in english.
    assert_equal 4 + 1, Geocoding::REGIONS_OF_BRAZIL["Centro Oeste"].size
  end

  test "should get region of state" do
    state = "Pernambuco"
    assert_equal "Nordeste", Geocoding.get_region_of_state(state)
    assert_nil Geocoding.get_region_of_state("Invalid State")
  end
end
